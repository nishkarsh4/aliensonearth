#include <iostream>
#include "include/alien.h"
#include "include/alienregistrar.h"

using namespace std;

int main()
{
    Alien alien;    //Create a new alien
    AlienRegistrar alienregistrar;  //Create a new alien registrar
    alienregistrar.register_alien(alien);   //Registers the alien
    return 0;
}
