#include "../include/detailsexporter.h"
#include "../include/formatwriter.h"


/* exportdetails method accepts a filled up details form as first parameter, gets a handle to object of desired
  format type as second and uses dynamic polymorphism to resolve which write() method to call */
void DetailsExporter::exportdetails(DetailsForm *detailsform, FormatWriter *formatwriter)
{
    formatwriter->write(detailsform);   //based on which subclass' object formatwriter points to, the respective write() is called
}
