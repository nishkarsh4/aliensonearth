#include "../include/alienregistrar.h"
#include "../include/alien.h"
#include "../include/detailsexporter.h"
#include "../include/formatselector.h"

void AlienRegistrar::register_alien(Alien alien)
{
    detailsform=new DetailsForm();  //creates a new detailsform

    //fills up the detailsform
    detailsform->setcode_name(alien.getcode_name());
    detailsform->setblood_color(alien.getblood_color());
    detailsform->setnum_antennas(alien.getnum_antennas());
    detailsform->setnum_legs(alien.getnum_legs());
    detailsform->sethome_planet(alien.gethome_planet());

    //calls the exportdetails method
    DetailsExporter::exportdetails(detailsform, FormatSelector::getselectedformat());
}
