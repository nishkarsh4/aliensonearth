#include "../include/pdfwriter.h"
#include "../include/detailsform.h"

#include <hpdf.h>   //  libharu include file for pdf

#include <cstring>
#include <iostream>

using namespace std;

/** write() method is the implementation of how the details are saved in pdf format, an open source pdf handling library -
    libharu is used for pdf handling */

void PDFWriter::write(DetailsForm *detailsform)
{
    HPDF_Doc pdf;
    pdf = HPDF_New(NULL, NULL); //initializes the pdfdoc object
    if (!pdf) {
        cout<<"Could not create pdf object!";
    }

    HPDF_Page page;
    page = HPDF_AddPage(pdf);  //adds a page to the pdf file

    /* set compression mode */
    HPDF_SetCompressionMode(pdf, HPDF_COMP_ALL);

    /* set page mode to use outlines. */
    HPDF_SetPageMode(pdf, HPDF_PAGE_MODE_USE_OUTLINE);

    /* create default-font */
    HPDF_Font font = HPDF_GetFont(pdf, "Helvetica", NULL);

    HPDF_Page_SetFontAndSize(page, font, 15);

    HPDF_Page_BeginText(page);  //begins a text object
    HPDF_Page_TextOut (page, (HPDF_Page_GetWidth(page)-50)/2, HPDF_Page_GetHeight(page) - 20, "Alien Details");

    HPDF_Page_SetFontAndSize(page, font, 10);

    HPDF_Page_TextOut (page, 1, HPDF_Page_GetHeight(page) - 30, "Code Name: ");
    HPDF_Page_ShowText(page, detailsform->getcode_name());

    HPDF_Page_MoveTextPos2(page, 0, -15);   //move the cursor to new x,y by adding given x,y to previous values
    HPDF_Page_ShowText(page, "Blood Color: ");
    HPDF_Page_ShowText(page, detailsform->getblood_color());

    char num_legs[10];
    itoa(detailsform->getnum_legs(),num_legs,10);   //converts from int to ascii, a null terminated string, stores it to num_legs
    HPDF_Page_MoveTextPos2(page, 0, -15);
    HPDF_Page_ShowText(page, "Number of Legs: ");
    HPDF_Page_ShowText(page, num_legs);

    char num_antennas[10];
    itoa(detailsform->getnum_antennas(),num_antennas,10);
    HPDF_Page_MoveTextPos2(page, 0, -15);
    HPDF_Page_ShowText(page, "Number of Antennas: ");
    HPDF_Page_ShowText(page, num_antennas);

    HPDF_Page_MoveTextPos2(page, 0, -15);
    HPDF_Page_ShowText(page, "Home Planet: ");
    HPDF_Page_ShowText(page, detailsform->gethome_planet());

    HPDF_Page_EndText(page);    //ends the text object

    HPDF_SaveToFile(pdf, "alien.pdf");  //saves the file
    HPDF_Free(pdf); //free the resources held by pdf object
    cout<<"\nDetails written successfully to alien.pdf";
}
