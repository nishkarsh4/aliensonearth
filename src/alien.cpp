#include "../include/alien.h"
#include <iostream>

using namespace std;

/*  Constructor to get the details of a new alien, these are the complete details an alien will have and not necessarily the only
    details required for registration */

Alien::Alien()
{
    cout<<"Code Name: ";
    cin.getline(code_name,30);
    cout<<"Blood Color: ";
    cin.getline(blood_color,20);
    cout<<"Number of Legs: ";
    cin>>num_legs;
    cout<<"Number of Antennas: ";
    cin>>num_antennas;
    cin.ignore();   //flush the remaining newline
    cout<<"Home Planet: ";
    cin.getline(home_planet,30);
}
