#include "../include/textwriter.h"
#include <fstream>
#include <iostream>

using namespace std;

/** write() method is the implementation of how details are written to a text file */

void TextWriter::write(DetailsForm *detailsform)
{
    fstream textfile;
    textfile.open("alien.txt",ios::out);
    textfile<<"Code Name: "<<detailsform->getcode_name()<<endl;
    textfile<<"Blood Color: "<<detailsform->getblood_color()<<endl;
    textfile<<"Number of legs: "<<detailsform->getnum_legs()<<endl;
    textfile<<"Number of Antennas: "<<detailsform->getnum_antennas()<<endl;
    textfile<<"Home Planet: "<<detailsform->gethome_planet()<<endl;
    textfile.close();
    cout<<"Details written successfully to alien.txt";
}
