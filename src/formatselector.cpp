#include <iostream>
#include <cstdlib>

#include "../include/formatselector.h"
#include "../include/textwriter.h"
#include "../include/pdfwriter.h"

using namespace std;

FormatWriter* FormatSelector::getselectedformat()
{
    int format;
    cout<<"\n\nSelect Format-\n";
    cout<<"1.Plain Text\n";
    cout<<"2.PDF\n";
    cout<<"Which?: ";
    cin>>format;
    switch(format)
    {
        case 1:
            return new TextWriter;  //returns a pointer to a TextWriter object
        case 2:
            return new PDFWriter;   //returns a pointer to a PDFWriter object
        default:
            cout<<"Format unavailable! Could not export! exiting...";
            exit(1);
    }
    return NULL;    //returns null if wrong case selected
}
