#ifndef DETAILSEXPORTER_H
#define DETAILSEXPORTER_H

#include "detailsform.h"
#include "formatwriter.h"

/* DetailsExporter class has a static method that exports the detailsform to the format it receives as parameter */

class DetailsExporter
{
    public:
        static void exportdetails(DetailsForm *detailsform, FormatWriter *formatwriter);
};

#endif // DETAILSEXPORTER_H
