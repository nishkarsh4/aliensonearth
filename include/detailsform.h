#ifndef DETAILSFORM_H
#define DETAILSFORM_H

#include <string.h>

/** It would look like as a redundancy here first, the members of DetailsForm class are exactly same as Alien class; However,
    The DetailsForm class only contains that information that would ever be needed for registration. Alien class can have many
    members, i.e. all the information and behaviour that Aliens have; the DetailsForm class would only have those that are
    needed for registration process */

class DetailsForm
{
    private:
        char code_name[30];
        char blood_color[20];
        unsigned int num_legs;
        unsigned int num_antennas;
        char home_planet[30];
    public:
        //setter methods
        void setcode_name(char *code_name)
        {
            strcpy(this->code_name, code_name);
        }
        void setblood_color(char *blood_color)
        {
            strcpy(this->blood_color,blood_color);
        }
        void setnum_legs(unsigned int num_legs)
        {
            this->num_legs=num_legs;
        }
        void setnum_antennas(unsigned int num_antennas)
        {
            this->num_antennas=num_antennas;
        }
        void sethome_planet(char *home_planet)
        {
            strcpy(this->home_planet,home_planet);
        }

        //getter methods
        char* getcode_name() { return code_name; }
        char* getblood_color() { return blood_color; }
        unsigned int getnum_legs() { return num_legs; }
        unsigned int getnum_antennas() { return num_antennas; }
        char* gethome_planet() { return home_planet; }
};

#endif // DETAILSFORM_H
