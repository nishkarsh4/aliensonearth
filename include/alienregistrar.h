#ifndef ALIENREGISTRAR_H
#define ALIENREGISTRAR_H

#include "detailsform.h"
#include "alien.h"

/** Alien Registrar can be thought of a person who has a desk and a details form that he fills up by asking the alien it's
    details */

class AlienRegistrar
{
    private:
        DetailsForm *detailsform;       //a pointer to detailsform object
    public:
        void register_alien(Alien alien);
};

#endif // ALIENREGISTRAR_H
