#ifndef FORMATWRITER_H
#define FORMATWRITER_H

#include "detailsform.h"

/** FormatWriter is a dependency for all the formats to which details may be exported. Its an abstract class */

class FormatWriter
{
    public:
        virtual void write(DetailsForm *detailsform)=0;     //Pure virtual method write, to be overridden in every subclass
};

#endif // FORMATWRITER_H
