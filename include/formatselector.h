#ifndef FORMATSELECTOR_H
#define FORMATSELECTOR_H

class FormatWriter;
class FormatSelector
{
    public:
        static FormatWriter* getselectedformat();
};

#endif // FORMATSELECTOR_H
