#ifndef ALIEN_H
#define ALIEN_H

/** The Alien class contains all the information and behaviour of Alien, in this case only the details we require for registration
    are kept */

class Alien
{
    private:
        char code_name[30];
        char blood_color[20];
        unsigned int num_legs;
        unsigned int num_antennas;
        char home_planet[30];

    public:
        Alien();    //Constructor
        char* getcode_name() { return code_name; }
        char* getblood_color() { return blood_color; }
        unsigned int getnum_legs() { return num_legs; }
        unsigned int getnum_antennas() { return num_antennas; }
        char* gethome_planet() { return home_planet; }
};

#endif
