#ifndef PDFWRITER_H
#define PDFWRITER_H

#include "formatwriter.h"

/* PDFWriter is dependent on FormatWriter */

class PDFWriter : public FormatWriter
{
    public:
        void write(DetailsForm *detailsform);   //implementation of how the details would be written in a pdf file
};

#endif // PDFWRITER_H
