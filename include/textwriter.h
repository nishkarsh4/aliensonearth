#ifndef TEXTWRITER_H
#define TEXTWRITER_H

#include "formatwriter.h"


/* TextWriter is dependent on FormatWriter */


class TextWriter : public FormatWriter
{
    public:
        void write(DetailsForm *detailsform);    //implementation of how the details would be written in a text file
};

#endif // TEXTWRITER_H
